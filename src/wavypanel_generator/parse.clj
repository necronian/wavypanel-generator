(ns wavypanel-generator.parse
  (:require [clojure.string :as string]
            [clojure.java.io :as io]
            [instaparse.core :as insta]))

(def parse-bpp
  "Returns the parse tree for a bpp file."
  (partial (insta/parser (slurp (io/resource "bpp-grammar.ebnf"))
                         :optimize :memory)))

(defn transform-bpp
  [coll]
  (insta/transform {:commandheader (fn [c & args]
                                     (into [(keyword c)]  args))
                    :commandvalues (fn [& args]
                                     (into [] args))
                    :command (fn [header values]
                               [(first header) (into [] (rest header)) values])}
                   coll))

(defn parse-file
  [s]
  (-> s
      slurp
      parse-bpp
      transform-bpp))

(defn extract-description
  [coll]
  (->> (rest coll)
       (filter #(= :description (first %)))
       first
       rest
       (map string/trim)))
