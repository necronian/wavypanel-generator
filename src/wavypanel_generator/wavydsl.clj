(ns wavypanel-generator.wavydsl
  (:gen-class)
  (:require [wavypanel-generator.dsl :as dsl]))

(def arc-with-3-points (partial dsl/ARC_IPEP))
(def random-seed (atom (java.util.Random. 12345)))

(defn generate-z-value
  [z min max neg]
  (let [random (.nextDouble @random-seed)
        step (with-precision 10 (/ (+ min max) 2))
        value (if neg
                (* step random)
                (* step random -1))]
    (cond (> min (+ z value)) (generate-z-value z min max neg)
          (< max (+ z value)) (generate-z-value z min max neg)
          :else value)))

(defn generate-x-value
  [current min max]
  (let [random (.nextDouble @random-seed)
        step (with-precision 10 (/ (+ min max) 2))
        value (* step random)]
    (cond (> min value) (generate-x-value current min max)
          (< max value) (generate-x-value current min max)
          :else (+ current value))))

(defn generate-y-value
  [center variance]
  (let [random (.nextDouble @random-seed)
        value (+ (- center variance) (* random (* variance 2)))]
    (cond (> (- center variance) value) (generate-y-value variance center)
          (< (+ center variance) value) (generate-y-value variance center)
          :else value)))

(defn generate-arc-segment
  [length x y z z-min z-max x-min x-max y-variance neg]
  (let [ze (generate-z-value z z-min z-max neg)
        xf2 (generate-x-value x x-min x-max)
        y2 (generate-y-value y y-variance)
        xfe (generate-x-value xf2 x-min x-max)
        xe (if (> length xfe) xfe length)
        x2 (if (> length xfe) xf2 (+ x (with-precision 10 (/ (- length x) 2))))
        ye (generate-y-value y y-variance)]
    (let [output {:command (arc-with-3-points :x2 x2 :y2 y2 :xe xe :ye ye :ze ze)
                  :x xe
                  :z (+ z ze)
                  :negative (if neg nil true)}]
      output)))

(defn generate-wavy-line
  [y length z-min z-max x-min x-max y-variance]
  (loop [arcs [(generate-arc-segment length
                                     0 y 0
                                     z-min z-max
                                     x-min x-max
                                     y-variance
                                     true)]]
    (if-not (= length (:x (last arcs)))
      (recur (conj arcs (generate-arc-segment length
                                              (:x (last arcs)) y (:z (last arcs))
                                              z-min z-max
                                              x-min x-max
                                              y-variance
                                              (:negative (last arcs)))))
      (map :command arcs))))

(defn milling
  [num & [content]]
  (-> [(dsl/rout num)]
      (into content)
      (into [(dsl/end)])))

(defn generate-wavy-milling
  [{:keys [geom-id y z-min z-max lpx lpy lpz
           x-min x-max y-variance seed] :as args}]
  (reset! random-seed (java.util.Random. seed))
  (let [arcs (->> (range (with-precision 10 (/ lpy y)))
                  (map #(into [(dsl/start-point :y (* y %))]
                              (generate-wavy-line (* % y)
                                                  lpx
                                                  z-min z-max x-min x-max
                                                  y-variance)))
                  flatten)]
    (milling geom-id arcs)))

(defn build-bpp
  [coll & {:keys [lpx lpy lpz description]
           :or {description nil}}]
  (->> dsl/footer-data
       (into coll)
       (into dsl/bpp-program)
       (into (dsl/bpp-variables :lpx lpx :lpy lpy :lpz lpz))
       (into (dsl/bpp-description description))
       (into dsl/bpp-header)
       (interpose "\n")
       (apply str)))
