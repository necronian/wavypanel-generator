(ns wavypanel-generator.core
  (:gen-class)
  (:require [seesaw.core :refer :all]
            [seesaw.chooser :refer :all]
            [clojure.string :as string]
            [clojure.edn :as edn]
            [wavypanel-generator.wavydsl :as w]
            [wavypanel-generator.parse :as parse]))

(def panel (atom nil))

(def generate-button (button :text "Generate Panel"))
(def save-button (button :text "Save"))
(def load-button (button :text "Load Settings"))

(defn do-to-map [amap keyseq f] (reduce #(assoc %1 %2 (f (%1 %2))) amap keyseq))

(defn generate-wavy-milling-seesaw-connector
  [args]
  (let [{:keys [geom-id start-id y z-min z-max lpy lpy lpz
                x-min x-max y-variance seed y-step] :as args}
        (do-to-map args [:y :z-min :z-max :lpx :lpy :lpz
                         :x-min :x-max :y-variance :seed] bigdec)]
    (reset! panel (w/generate-wavy-milling args))))

(defmacro get-version []
  (System/getProperty "wavypanel-generator.version"))

(def version-mappings
  {"0.2.0" {:seed "Random Seed"
            :lpx "LPX"
            :lpy "LPY"
            :lpz "LPZ"
            :geom-id "Geometry Id"
            :y "Nominal Distance Between Lines"
            :y-variance "Max Y Up/Down Movement"
            :z-min "Z-Min Depth"
            :z-max "Z-Max Depth"
            :x-min "Min X Segment Length"
            :x-max "Max X Segment Length"
            :version "Version"}})

(def version-defaults
  {"0.2.0" {:seed "12345"
            :lpx 800
            :lpy 600
            :lpz 19.05
            :geom-id "WavyMilling"
            :y 14
            :y-variance 5
            :z-min 1
            :z-max 8
            :x-min 25
            :x-max 75}})

(defn build-items
  "Combine the defaults and mappings maps in order to create a vector of seesaw
  widgets to include in the main grid panel"
  []
  (let [defaults (get version-defaults (get-version))
        mapping (get version-mappings (get-version))]
    (->> defaults
         (map (fn [[k v]] [(k mapping) (text :id k :text v)]))
         flatten
         (into []))))

(def p (grid-panel
        :columns 2
        :items (conj (build-items) generate-button save-button load-button)))

(defn generate-button-onclick
  [e]
  (generate-wavy-milling-seesaw-connector (value p))
  (alert e "Finished Generation"))

(defn build-description
  []
  (let [mapping (get version-mappings (get-version))]
    (->> (value p)
         (merge {:version (get-version)})
         (map (fn [[k v]]
                (str (k mapping) " - " v))))))

(defn parse-description
  [coll]
  (let [data (map #(string/split % #" - ") coll)
        version (->> data
                     (filter (fn [[k v]] (= k "Version")))
                     first
                     rest
                     first)
        mapping (get version-mappings version)]
    (->> data
         (map (fn [[k v]]
                {(->> mapping
                      (filter (fn [[dk dv]] (when (= dv k) dk))) first first)
                 v}))
         (apply merge))))

(defn save-button-onclick
  [e]
  (let [file (choose-file :type :save
                          :all-files? true
                          :filters [["Biesse" ["bpp"]]])
        {:keys [lpy lpx lpz seed geom-id y y-variance z-min z-max x-min x-max]}
        (value p)
        description (build-description)]
    (when file
      (let [filename (if (re-find #"\.bpp$" (str file))
                       file
                       (clojure.java.io/file (str file ".bpp")))]
        (spit filename (w/build-bpp @panel
                                    :lpy lpy
                                    :lpx lpx
                                    :lpz lpz
                                    :description description))))))

(defn load-button-onclick
  [e]
  (let [file (choose-file :type :open
                          :all-files? true
                          :filters [["Biesse" ["bpp"]]])]
    (when file (->> file
                    parse/parse-file
                    parse/extract-description
                    parse-description
                    (value! p)))))

(defn setup-button-listeners []
  (listen generate-button :action generate-button-onclick)
  (listen save-button :action save-button-onclick)
  (listen load-button :action load-button-onclick))

(defn -main
  [& args]
  (invoke-later
   (-> (frame :title "WavyPanel Generator",
              :content p,
              :on-close :exit)
       pack!
       show!))
  (setup-button-listeners))
