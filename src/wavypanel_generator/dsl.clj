(ns wavypanel-generator.dsl
  (:require [clojure.string :as string]))

(def milling-id (atom 10000000))

(def arc-direction-types {:clockwise 1 :counter-clockwise 2})

(def tool-types {:candela 100 :rout0 102 :rout1 103 :sagomata 101})

(def correction-types {:left 1 :central 0 :right 2})

(def lead-types {:tglinecurve 3 :helix 5 :corrected-3d-line 7 :line 2
                 :corrected-line 9 :arc 1 :corrected-3d-curve 8})

(def repetition-types {:none -1 :repeat-in-x 0 :repeat-in-y 2 :circular-repeat 3
                       :straight-repeat-in-length-and-angle 5})

;;0=off 1=sc1 MAYBE??? at least on off, sc1 and sc2 seem to do nothing different
(def sharp-corner-types {:off 0 :sc1 1 :sc2 2})

(defn get-id
  "Returns an id and increments the global id atom"
  []
  (let [id @milling-id] (swap! milling-id inc) id))

(defn quote-str [s] (str "\"" s "\""))

(defn command-header
  "This header is the same for all drawing commands"
  [command & {:keys [description] :or {description ""}}]
  (string/join
   " "
   ["@"                            ; Denote the beginning of a command
    (string/join
     ", "
     [command                      ; The command name, eg ISO, LINE_EP, ROUT
      (quote-str nil)              ; UNKNOWN TDCODE7 TDCODE88 TDCODE9 ???? Seems
                                        ; to mostly show up in geometry and boring
                                        ; operations
      (quote-str description)      ; This is the description of the operation
                                        ; that show up in the program sidebar
      (get-id)
      (quote-str nil)              ; UNKNOWN obviously a string of some type
      0])                          ; UNKNOWN ???? Seemss to always be zero. I
                                        ; assume that it is the side to be operated
                                        ; on 0 would be top
    ": "]))                        ; Seperate the header from the arguments

(defn build-command
  "Join the command header with the joined arguments"
  [cmd coll]
  (->> coll
       (string/join ", ")
       (str (command-header cmd))))

(defn end [] (command-header "ENDPATH"))

(defn start-point
  [& {:keys [x y z] :or {x 0 y 0 z 0}}]
  (build-command "START_POINT" [x y z]))

(def bpp-header ["[HEADER]" "TYPE=BPP" "VER=150"])

(defn bpp-description
  "Given a collection of strings build the description for the bpp"
  [coll]
  (if (seq coll)
    (->> coll
         (map #(str "| " %))
         (into ["[DESCRIPTION]"]))
    ["[DESCRIPTION]" "| "]))

(def bpp-variable-location
  {:global "GLB"
   :local "LOC"
   :system "PAN"}) ; I don't know what pan stands for 

(def bpp-variable-types {:angle 6
                         :distance 4
                         :percent 8
                         :real 2
                         :speed 5
                         :string 3
                         :time 7
                         :whole 1
                         :generic 0})

(defn bpp-variable
  "Build a variable for placement in the bpp variables section"
  [{:keys [location name value description type]
    :or {location :local description "" type :generic}}]
  (->> [(location bpp-variable-location)
        "="
        name
        "|"
        (if (= :string type) (quote-str value) value)
        "|"
        description
        "|"
        (type bpp-variable-types)
        "|"
        ]
       (apply str)))

(defn bpp-variables
  [& {:keys [lpx lpy lpz]
      :or {lpx 600 lpy 600 lpz 30}}]
  ["[VARIABLES]"
   (bpp-variable {:location :system :value lpx :name "LPX" :type :distance})
   (bpp-variable {:location :system :value lpy :name "LPY" :type :distance})
   (bpp-variable {:location :system :value lpz :name "LPZ" :type :distance})
   "PAN=ORLST|\"\"||3|"
   "PAN=SIMMETRY|0||1|"
   "PAN=TLCHK|0||1|"
   "PAN=TOOLING|\"\"||3|"
   "PAN=CUSTSTR|$B$KBsExportToNcRvA.XncExtraPanelData$V\"\"||3|"
   "PAN=FCN|1.000000||2|"
   "PAN=XCUT|0||4|"
   "PAN=YCUT|0||4|"
   "PAN=JIGTH|0||4|"
   "PAN=CKOP|0||1|"
   "PAN=UNIQUE|0||1|"
   "PAN=MATERIAL|\"wood\"||3|"
   "PAN=PUTLST|\"\"||3|"
   "PAN=OPPWKRS|0||1|"
   "PAN=UNICLAMP|0||1|"
   "PAN=CHKCOLL|0||1|"
   "PAN=WTPIANI|0||1|"
   "PAN=COLLTOOL|0||1|"
   "PAN=CALCEDTH|0||1|"
   "PAN=ENABLELABEL|0||1|"
   "PAN=LOCKWASTE|0||1|"
   "PAN=LOADEDGEOPT|0||1|"
   "PAN=ITLTYPE|0||1|"
   "PAN=RUNPAV|0||1|"
   "PAN=FLIPEND|0||1|"])

(def bpp-program ["[PROGRAM]"])

(def footer-data ["[VBSCRIPT]" "[MACRODATA]" "[TDCODES]"
                  "[PCF]" "[TOOLING]" "[SUBPROGS]"])

(defn ARC_IPEP
  "Draw an arc with 3 points 1st point is determined by the last point drawn"
  [& {:keys [x2 y2 xe ye zs ze sharpcorner workspeed rotationspeed]
      :or {x2 0 y2 0 xe 0 ye 0 zs 0 ze 0 sharpcorner 0 workspeed 0
           rotationspeed 0}}]
  (build-command "ARC_IPEP" [x2 y2 xe ye zs ze sharpcorner workspeed
                             rotationspeed 0]))

(defn LINE_EP
  "Draw a line given an endpoint"
  [& {:keys [x y zs ze sharpcorner workspeed rotationspeed mvtrepositioning]
      :or {x 0 y 0 zs 0 ze 0 sharpcorner 0 workspeed 0 rotationspeed 0
           mvtrepositioning 0}}]
  (build-command "LINE_EP" [x y zs ze sharpcorner workspeed rotationspeed 0
                            mvtrepositioning])) ;WTF IS MVTREpositioning?

(defn LINE_LNAN
  "Draw a line given a length and angle"
  [& {:keys [length angle zs ze sharpcorner workspeed rotationspeed]
      :or {length 0 angle 0 zs 0 ze 0 sharpcorner 0 workspeed 0
           rotationspeed 0}}]
  (build-command "LINE_LNAN" [length angle zs ze sharpcorner workspeed
                              rotationspeed 0]))

(defn ARC_EPCE
  "Arc With End Point and Center"
  [& {:keys [x y xc yc zs ze sharpcorner workspeed rotationspeed arcdirection]
      :or {x 0 y 0 xc 0 yc 0 zs 0 ze 0 sharpcorner 0 workspeed 0 rotationspeed 0
           arcdirection 1}}]
  (build-command "ARC_EPCE" [x y xc yc arcdirection zs ze sharpcorner workspeed
                             rotationspeed 0]))

(defn ARC_EPRA
  "Arc With End Point and Radius"
  [& {:keys [x y radius zs ze sharpcorner workspeed rotationspeed arcdirection
             solution]
      :or {x 0 y 0 radius 0 zs 0 ze 0 sharpcorner 0 workspeed 0 rotationspeed 0
           arcdirection 1 solution 0}}]
  (build-command "ARC_EPRA" [x y radius arcdirection zs ze sharpcorner workspeed
                             rotationspeed solution]))

(defn rout
  [geometry_id & {:keys [bit-diameter corners tool-name tool-type correction
                         lead-in lead-in-angle lead-out lead-out-angle depth
                         repetition]
                  :or {bit-diameter 16.75
                       corners "1"
                       tool-name "FINGERPULL"
                       tool-type 101
                       correction 0
                       lead-in 9
                       lead-in-angle 45
                       lead-out 9
                       lead-out-angle 45
                       depth 0
                       repetition -1}}]
  (build-command "ROUT"
                 [(quote-str geometry_id) ;Geometry Id
                  0
                  (quote-str corners)     ;CORNERS "1,2,3,4"
                  0                       ;ZCHANNEL
                  depth                   ;DEPTH
                  (quote-str nil)         ;ISO CODE
                  1
                  bit-diameter            ;DIAMATER
                  repetition
                  0                       ;CENTER OF REPEAT ROTATION  X
                  0                       ;CENTER OF REPEAT ROTATION Y
                  32                      ;DXSTEP
                  32                      ;DYSTEP
                  50                      ;RADIUS OF REPEAT FOR CIRCULAR REPEAT
                  0                       ;STARTING ANGLE FOR RADIUS REPEAT
                  45                      ;ANGULARSTEP FOR CIRCULAR REPEAT
                  1                       ;RADIAL REPEAT FOR CIRCULAR REPEAT
                  0                       ;NUMBER REPEAT
                  0                       ;AZ ABS
                  0                       ;AR ABS
                  0                       ;STARTING Z
                  0                       ;ENDING Z
                  0
                  0                       ;THROUGH MACHINING
                  0                       ;REVERSE
                  0                       ;ENABLE  TCP BOOLEAN
                  0                       ;REPEATING ANGLE
                  0                       ;REPEATING LENGTH STEP
                  1                       ;CIRCULAR REPEAT BOOLEAN FIRST ITEM
                  0
                  0                       ;OVER MATERIAL
                  0
                  0
                  0                       ;VERTICAL RUNS
                  0                       ;FINAL VERTICAL RUN
                  0                       ;HORIZONTAL RUNS
                  0                       ;HORIZONTAL STEP
                  0                       ;ENABLE FINISHING STEP
                  0                       ;FINISHINGSTEP
                  1
                  0
                  -1
                  0                       ;ADDITIONAL Z SAFTEY
                  0                       ;LOWERING SPEED
                  0                       ;ROTATIONAL SPEED
                  0                       ;APPROX OUTPUT SPEED
                  0                       ;WORKSPEED
                  (quote-str tool-name)
                  tool-type
                  1
                  correction
                  lead-in
                  lead-in-angle
                  0                       ;LEAD IN CORRECTION IN AIR BOOLEAN
                  0                       ;LEAD IN GIN OFFSET
                  0                       ;LEAD IN INSERT TABBING
                  0                       ;LEAD IN TABBING LENGTH
                  0                       ;LEAD IN TABBING POSITION
                  lead-out
                  lead-out-angle
                  0                       ;LEAD OUT CORRECTION IN AIR 0=false 1=true
                  0                       ;LEAD OUT GIN OFFSET
                  0                       ;LEAD OUT INSERT TABBING BOOLEAN??
                  0                       ;LEAD OUT TABBING
                  0                       ;LEAD OUT TABBING POSITION
                  0                       ;INITIAL EXT
                  0                       ;FINAL EXT
                  0                       ;DECELEARATION DISTANCE
                  0                       ;PERCENT RADIUS
                  0                       ;ALLOW TWO WAY RUNS BOOLEAN
                  (quote-str nil)         ;SPINDEL SELECT - WE HAVE 1 TP1 - DEFAULT ""
                  0                       ;SHARP CORNERS BOOLEAN
                  0                       ;FLOATING BOOLEAN
                  0                       ;BLOWER BOOLEAN
                  0                       ;PRESSER BOOLEAN
                  1                       ;CHIP CLEANING BY BLOWING??? BOOLEAN
                  0                       ;HOODPOSITION
                  0
                  0                       ;CORNERSPEED
                  0
                  (quote-str nil)
                  5
                  0
                  20
                  80
                  60
                  0
                  (quote-str nil)
                  (quote-str nil)
                  (quote-str "ROUT")      ;COMMANDN NAME AGAIN???
                  0
                  1
                  1
                  0
                  0
                  0
                  0
                  0
                  0]))                    ;AVOID OUTPUT BETWEEN THE RUNS
