(defproject wavypanel-generator "0.2.0"
  :description "Generates wavy millings and exports to bpp for biesse cnc machines"
  :url "https://bitbucket.org/necronian/wavypanel-generator/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [seesaw "1.4.5"]
                 [instaparse "1.3.5"]]
  :main wavypanel-generator.core
  :profiles {:dev {:plugins [[cider/cider-nrepl "0.9.0-SNAPSHOT"]]}
             :uberjar {:aot :all}})
